# Avanti Gurukul, code name Spiderweb

This is Avanti Online Student Android App

## Checklist when releasing build for testing

* Disable debug logging
* Make sure staging is on for next 2-3 days when releasing a build
* Obfuscate code

## Branch creation rules

Key points:
* A new product feature (like Quiz) will have at least one main branch (Quiz) for that product feature
  * That branch will be protected - no one will be able to push to that branch without a pull request
  * This will be the branch which should be tested on by non-devs for the current state of the full feature
* New individual dev branches will be created from the main feature branch for individual features (whether big or small)
  * Code review MUST happen at the time of the first Pull Request from the feature branch into a protected branch
* Ideally, these individual branches should be deleted once the pull request has been approved
* Two developers should ideally never need to work on the same branch simultaneously
* `master` branch will **always be stable** and in a **release-ready state**

## Linking to common learn module

* Clone learn repository from https://bitbucket.org/pritamsukumar/learn/src/master/
* Clone this spiderweb_sankalp repository (https://bitbucket.org/pritamsukumar/spiderweb_sankalp/src/master/) also in the same directory where learn repository is cloned
* In android studio open project spiderweb_sankalp.
* Perform clean project
