package `in`.avanti.studentcompanion.presenters

import `in`.avanti.studentcompanion.AppConfig
import `in`.avanti.studentcompanion.R
import `in`.avanti.studentcompanion.accounts.AccountManager
import `in`.avanti.studentcompanion.mixpanel.MixpanelManager
import `in`.avanti.studentcompanion.models.Product
import `in`.avanti.studentcompanion.models.Student
import `in`.avanti.studentcompanion.realm.RealmManager
import `in`.avanti.studentcompanion.rest.client.GenericClient
import `in`.avanti.studentcompanion.rest.client.GrahakApiClient
import `in`.avanti.studentcompanion.rest.model.RegisterUserRequestParameters
import `in`.avanti.studentcompanion.rest.model.SankalpRegisterUserRequestParameters
import `in`.avanti.studentcompanion.utils.*
import `in`.avanti.studentcompanion.views.activities.OtpActivity
import `in`.avanti.studentcompanion.views.activities.SankalpMainActivity
import `in`.avanti.studentcompanion.views.activities.SankalpOtpActivity
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException

private const val PRODUCT_SUFFIX = "_nolang"

class SankalpRegisterPresenter(otpActivity: OtpActivity, mView: View, loginUser: Boolean) : RegisterPresenter(otpActivity, mView, loginUser) {

    private val TAG = SankalpRegisterPresenter::class.java.simpleName

    override fun buildLandingPresenter(): LandingPresenter {
        return SankalpLandingPresenter(mOtpActivity)
    }

    override fun buildUserData(name: String, otp: String): RegisterUserRequestParameters.UserData {
        return SankalpRegisterUserRequestParameters.SankalpUserData((mOtpActivity as SankalpOtpActivity).getGrade(), SankalpConstants.CURRICULUM_SANKALP,
                getUserDataBuilder(name, otp))
    }

    override fun deviceTokenRegisteredHandler(): InstanceIdManager.IDeviceTokenRegistered {
        return InstanceIdManager.IDeviceTokenRegistered {
            getProducts()
        }
    }

    override fun setProductTagSuffixIfNeeded(student: Student, userData: RegisterUserRequestParameters.UserData) {
        /**
         * ALL sankalp students on app WILL be assigned same product, hence hardcoding productTagSuffix;
         * otherwise it doesn't come in app_init call for registrations, hence remains null which if passed to
         * next call to set current product, then it throws 422 (unprocessable entity) error
         */
        student.productTagSuffix = (userData as SankalpRegisterUserRequestParameters.SankalpUserData).grade + PRODUCT_SUFFIX;
    }

    // TODO: Almost similar code is there in MainPresenter also. If possible, refactor to remove code duplication
    private fun getProducts() {
        val call: Call<List<Product>> = GenericClient.getInstance(AppConfig.getAllProductsUrl(), false, true)
                .apiService.allProducts
        call.enqueue(object : Callback<List<Product>> {
            override fun onResponse(@NonNull call: Call<List<Product>>, @NonNull response: Response<List<Product>>) {
                if (response.isSuccessful) {
                    val products: MutableList<Product> = response.body()!!.toMutableList()

                    /**
                     * Products with null metadata are considered invalid, hence don't save
                     * those in realm db
                     */
                    val iterator: MutableIterator<Product> = products.iterator()
                    while (iterator.hasNext()) {
                        if (Preconditions.isNull(iterator.next().metadata)) {
                            iterator.remove()
                        }
                    }
                    val realmManager = RealmManager.getInstance()
                    realmManager.saveAllProducts(products)
                    setCurrentProduct()

                } else {
                    Utility.hideLoading()
                    Log.d(TAG, "Error: could not fetch products." + response.message())
                }
            }

            override fun onFailure(@NonNull call: Call<List<Product>>, @NonNull t: Throwable) {
                Log.e(TAG, "Error: could not fetch products.", t)
                Utility.hideLoading()
            }
        })
    }

    // TODO: Somewhat similar code is there in GradePresenter also. If possible, refactor to remove code duplication
    private fun setCurrentProduct() {
        val productId: String = SankalpFirebaseRemoteConfigHelper.getActiveProducts().getValue((mOtpActivity as SankalpOtpActivity).getGrade())
        val student: Student = AccountManager.getInstance().loggedInStudent
        val call: Call<Void> = GrahakApiClient.getInstance().apiService.setCurrentProduct(productId, student.productTag)

        val realmManager = RealmManager.getInstance()
        val gradeName = realmManager.getProductNameByProductId(productId)

        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful) {
                    // hide spinner
                    Utility.hideLoading()
                    student.currentProductId = productId
                    student.gradeName = gradeName
                    student.gradeId = productId

                    val productMetaDataBean = realmManager.getProductById(productId).metadata
                    student.productCollectionId = productMetaDataBean.productsCollectionId
                    student.creditCollectionId = productMetaDataBean.creditsCollectionId
                    RealmManager.getInstance().saveStudent(student)

                    // Mixpanel: log successful "Onboarded" event
                    MixpanelManager.getInstance().onboardedEvent(true, null, gradeName, null, student,
                            response.code())
                    Utility.showToastMessageLong(mOtpActivity, "Successfully registered")

                    mOtpActivity.finish()
                    PreferenceHandler.setLoggedIn(true)
                    PreferenceHandler.writeBoolean(mOtpActivity, PreferenceHandler.GRADE_SELECTION_PENDING, false)
                    mOtpActivity.startActivity(Intent(mOtpActivity, SankalpMainActivity::class.java).setFlags(
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
                } else {
                    var message: String? = null
                    try {
                        val error = JSONObject(response.errorBody()?.string())
                        message = error.getString("message")
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    // Mixpanel: log unsuccessful "Onboarded" event
                    MixpanelManager.getInstance().onboardedEvent(false, message, gradeName, null,
                            student, response.code())
                    Utility.showToastMessageShort(mOtpActivity, response.message())
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                Utility.hideLoading()

                val message: String
                val errorCode: Int

                if (t is SocketTimeoutException) {
                    message = getMsg(R.string.error_connection_timeout)
                    errorCode = Constants.CONNECTION_TIME_OUT
                } else {
                    message = getMsg(R.string.error_server_error)
                    errorCode = Constants.SERVICE_UNAVAILABLE
                }
                Utility.showToastMessageShort(mOtpActivity, message)

                // Mixpanel: log unsuccessful "Onboarded" event
                MixpanelManager.getInstance().onboardedEvent(false, message, gradeName, null,
                        student, errorCode)
            }
        })
    }
}