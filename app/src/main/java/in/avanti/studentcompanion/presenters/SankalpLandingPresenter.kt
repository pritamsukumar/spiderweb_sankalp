package `in`.avanti.studentcompanion.presenters

import `in`.avanti.studentcompanion.models.SankalpStudent
import `in`.avanti.studentcompanion.models.Student
import `in`.avanti.studentcompanion.utils.PreferenceHandler
import android.content.Context

class SankalpLandingPresenter(activity: Context) : LandingPresenter(activity) {

    override fun chkForHardcodedBatchPrefix(student: Student) {
        if (student.batchCode.startsWith(SankalpStudent.BATCH_CODE_SANKALP_PREFIX)) {
            student.batchPartnerName = Student.BATCH_PARTNER_NAME_SANKALP
            PreferenceHandler.writeBoolean(activity, PreferenceHandler.BATCH_PARTNER_NAME_FIXED_USING_SERVICE, false)
        } else {
            super.chkForHardcodedBatchPrefix(student)
        }
    }
}