package `in`.avanti.studentcompanion.presenters

import `in`.avanti.studentcompanion.sankalp.R
import `in`.avanti.studentcompanion.utils.Constants
import `in`.avanti.studentcompanion.views.activities.BaseActivity
import `in`.avanti.studentcompanion.views.fragments.GeneralDialogFragment
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View

private const val DIALOG_TAG_ONLY_FOR_HARYANA = "onlyForHaryana"

class SankalpLoginPresenter(sankalpLoginActivity: Context, mView: View) : LoginPresenter(sankalpLoginActivity, mView) {

    private lateinit var generalDialogFragment: GeneralDialogFragment

    override fun buildLandingPresenter(): LandingPresenter {
        return SankalpLandingPresenter(mLoginActivity)
    }

    override fun onStudentDetailsFetched(loginMethod: String, responseCode: Int, phone: String?, email: String?) {
        if (!student.isSankalpStudent) {
            studentDetailsNotFetched(responseCode, getMsg(R.string.app_only_for_haryana_short),
                    loginMethod, phone, email)
            showAppOnlyForHaryanaDialog()

        } else {
            super.onStudentDetailsFetched(loginMethod, responseCode, phone, email)
        }
    }

    private fun showAppOnlyForHaryanaDialog() {
        generalDialogFragment = GeneralDialogFragment.Builder("Download Avanti Gurukul App",
                        mLoginActivity.getString(R.string.app_only_for_haryana), "Go to Play Store",
                        mLoginActivity.getString(R.string.cancel_string))
                .setPositiveBtnClickListener(View.OnClickListener {
                    generalDialogFragment.dismiss()
                    goToAvantiGurukulPlayStorePage()
                })
                .setNegativeBtnClickListener(View.OnClickListener { generalDialogFragment.dismiss() })
                .showAlert((mLoginActivity as BaseActivity).supportFragmentManager, DIALOG_TAG_ONLY_FOR_HARYANA)
    }

    private fun goToAvantiGurukulPlayStorePage() {
        val goToMarket = getIntentForUrl(Constants.PLAYSTORE_MARKET_BASE_URL)
        try {
            mLoginActivity.startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            mLoginActivity.startActivity(getIntentForUrl(Constants.PLAYSTORE_HTTP_BASE_URL))
        }
    }

    private fun getIntentForUrl(url: String): Intent {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, "in.avanti.gurukul.learning.cbse.ncert.iit.video.test.doubt")))
        // To count with Play market backstack, After pressing back button,
        // to take back to our application, we need to add following flags to intent.
        var flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        flags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            flags or Intent.FLAG_ACTIVITY_NEW_DOCUMENT
        } else {
            flags or Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET
        }
        intent.addFlags(flags)
        return intent
    }
}