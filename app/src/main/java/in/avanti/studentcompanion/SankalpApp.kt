package `in`.avanti.studentcompanion

import `in`.avanti.studentcompanion.adapters.TutorialAdapter
import `in`.avanti.studentcompanion.sankalp.BuildConfig
import `in`.avanti.studentcompanion.utils.Constants
import `in`.avanti.studentcompanion.utils.SankalpFirebaseRemoteConfigHelper
import `in`.avanti.studentcompanion.views.activities.*

class SankalpApp: StudentCompanion() {

    override fun configApp() {
        Constants.VERSION_CODE = BuildConfig.VERSION_CODE
        Constants.VERSION_NAME = BuildConfig.VERSION_NAME

        AppConfig.MIXPANEL_ANDROID_DEV_TOKEN = "8ab86d93d8e55fce724e75fe3c97d2e7"
        AppConfig.MIXPANEL_PRODUCTION_TOKEN = "c498dd59ff08fb53ef554be766216428"

        TutorialAdapter.SLIDER_COUNT = 1
        initActivities()
    }

    override fun buildFirebaseRemoteConfigDefaults(): MutableMap<String, Any> {
        val remoteConfigDefaults = super.buildFirebaseRemoteConfigDefaults()
        remoteConfigDefaults[SankalpFirebaseRemoteConfigHelper.KEY_ACTIVE_PRODUCTS] = SankalpFirebaseRemoteConfigHelper.DEFAULT_VAL_ACTIVE_PRODUCTS
        return remoteConfigDefaults
    }

    private fun initActivities() {
        Constants.ACTIVITY_LANDING = SankalpLandingActivity::class.java
        Constants.ACTIVITY_REGISTER_USER = SankalpRegisterUserActivity::class.java
        Constants.ACTIVITY_LOGIN = SankalpLoginActivity::class.java
        Constants.ACTIVITY_OTP = SankalpOtpActivity::class.java
        Constants.ACTIVITY_MAIN = SankalpMainActivity::class.java
    }
}