package `in`.avanti.studentcompanion.utils

import `in`.avanti.studentcompanion.AppConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject


class SankalpFirebaseRemoteConfigHelper: FirebaseRemoteConfigHelper() {

    companion object {

        internal const val KEY_ACTIVE_PRODUCTS = "active_products"
        internal const val DEFAULT_VAL_ACTIVE_PRODUCTS = "{\"staging\": {\"9\": \"2479651520570\", \"10\": \"2479761555514\", \"11\": \"2484062355514\", \"12\": \"2484067762234\"}, \"production\": {\"9\": \"2239175688265\", \"10\": \"2239179292745\", \"11\": \"2239180800073\", \"12\": \"2239183192137\"}}"

        fun getActiveProducts(): Map<String, String> {
            val jObjActiveProducts = JSONObject(FirebaseRemoteConfig.getInstance().getString(KEY_ACTIVE_PRODUCTS))
            val jStrActiveProducts = if (AppConfig.isProd()) {
                jObjActiveProducts.getString(KEY_PRODUCTION)
            } else {
                jObjActiveProducts.getString(KEY_STAGING)
            }
            return Gson().fromJson(jStrActiveProducts, object: TypeToken<Map<String, String>>() {}.type)
        }
    }
}