package `in`.avanti.studentcompanion.utils

class SankalpConstants {
    companion object {
        const val GRADE = "Grade"
        const val CURRICULUM_SANKALP = "Sankalp"
        const val SHARED_PREF_LOCATION_UPDATE_DONE = "LOCATION_UPDATE_DONE"
    }
}