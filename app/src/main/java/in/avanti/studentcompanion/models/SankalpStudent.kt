package `in`.avanti.studentcompanion.models

class SankalpStudent: Student() {
    companion object{
        const val BATCH_CODE_SANKALP_PREFIX = "SKP"
    }
}