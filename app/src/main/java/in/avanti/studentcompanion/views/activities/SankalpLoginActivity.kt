package `in`.avanti.studentcompanion.views.activities

import `in`.avanti.studentcompanion.presenters.SankalpLoginPresenter
import `in`.avanti.studentcompanion.presenters.LoginPresenter

class SankalpLoginActivity : LoginActivity() {

    override fun buildPresenter(): LoginPresenter {
        return SankalpLoginPresenter(this,  login_form)
    }
}
