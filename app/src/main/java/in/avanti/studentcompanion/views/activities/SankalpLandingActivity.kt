package `in`.avanti.studentcompanion.views.activities

import `in`.avanti.studentcompanion.presenters.LandingPresenter
import `in`.avanti.studentcompanion.presenters.SankalpLandingPresenter

class SankalpLandingActivity : LandingActivity() {

    override fun buildPresenter(): LandingPresenter {
        return SankalpLandingPresenter(this)
    }
}
