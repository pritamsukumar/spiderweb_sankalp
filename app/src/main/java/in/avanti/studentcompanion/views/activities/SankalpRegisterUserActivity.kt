package `in`.avanti.studentcompanion.views.activities

import `in`.avanti.studentcompanion.sankalp.R
import `in`.avanti.studentcompanion.utils.SankalpConstants
import `in`.avanti.studentcompanion.utils.Utility
import android.content.Intent
import kotlinx.android.synthetic.main.activity_register.*

class SankalpRegisterUserActivity: RegisterUserActivity() {

    override fun isValidUser(): Boolean {
        var validGrade = false
        val gradeString: String = grades_spinner.selectedItem.toString()
        if (gradeString != resources.getStringArray(R.array.grades)[0]) {
            validGrade = true
        } else {
            val message = resources.getString(R.string.please_select_grade)
            Utility.CustomTextSnackBar(
                    message,
                    register_form,
                    this)
        }
        return super.isValidUser() && validGrade
    }

    override fun buildOtpIntent(): Intent {
        return super.buildOtpIntent().putExtra(SankalpConstants.GRADE, grades_spinner.selectedItem.toString())
    }
}