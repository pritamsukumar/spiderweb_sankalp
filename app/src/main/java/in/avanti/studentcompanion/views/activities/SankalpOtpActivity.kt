package `in`.avanti.studentcompanion.views.activities

import `in`.avanti.studentcompanion.presenters.SankalpLoginPresenter
import `in`.avanti.studentcompanion.presenters.LoginPresenter
import `in`.avanti.studentcompanion.presenters.RegisterPresenter
import `in`.avanti.studentcompanion.presenters.SankalpRegisterPresenter
import `in`.avanti.studentcompanion.utils.EmptyUtils
import `in`.avanti.studentcompanion.utils.SankalpConstants
import android.os.Bundle

class SankalpOtpActivity : OtpActivity() {

    fun getGrade(): String {
        val extras: Bundle = intent.extras
        return if (EmptyUtils.isEmpty(extras)) "" else extras.getString(SankalpConstants.GRADE)
    }

    override fun buildLoginPresenter(): LoginPresenter {
        return SankalpLoginPresenter(this, lnrView)
    }

    override fun buildRegisterPresenter(): RegisterPresenter {
        return SankalpRegisterPresenter(this, lnrView, mUserLogin)
    }
}
