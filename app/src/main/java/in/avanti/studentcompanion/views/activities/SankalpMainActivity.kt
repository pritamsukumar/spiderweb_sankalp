package `in`.avanti.studentcompanion.views.activities

import `in`.avanti.studentcompanion.accounts.AccountManager
import `in`.avanti.studentcompanion.rest.client.GoogleSheetsScriptApiClient
import `in`.avanti.studentcompanion.sankalp.R
import `in`.avanti.studentcompanion.utils.GpsUtils
import `in`.avanti.studentcompanion.utils.PreferenceHandler
import `in`.avanti.studentcompanion.utils.SankalpConstants
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val LOCATION_REQUEST = 1000

class SankalpMainActivity: MainActivity() {

    private val LOG_TAG = SankalpMainActivity::class.java.simpleName

    //Location related fields
    private lateinit var mFusedLocationClient: FusedLocationProviderClient

    private var wayLatitude = 0.0
    private var wayLongitude = 0.0
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var isGPSOn = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        updateLocation()
    }

    /*
    To update location of student so that we can get an idea of their school location.
    This is a temporary solution because adding a dropdown list of 5000 schools is not practical

    Ref: https://medium.com/@droidbyme/get-current-location-using-fusedlocationproviderclient-in-android-cb7ebf5ab88e
     */
    private fun updateLocation() {

        // For now we will only get the student's location once
        if (PreferenceHandler.readBoolean(this, SankalpConstants.SHARED_PREF_LOCATION_UPDATE_DONE, false)) {
            return
        }

        GpsUtils(this).turnGPSOn { isGPSEnable ->
            // turn on GPS
            isGPSOn = isGPSEnable
        }

        if (!isGPSOn) {
            return
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 10 * 1000.toLong() // 10 seconds
        locationRequest.fastestInterval = 5 * 1000.toLong() // 5 seconds

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        wayLatitude = location.latitude
                        wayLongitude = location.longitude

                        mFusedLocationClient.removeLocationUpdates(locationCallback)
                        updateLocationSpreadsheet()
                    }
                }
            }
        }

        getLocation()
    }

    /*
    Adds location to spreadsheet:

    Link to spreadsheet: https://docs.google.com/spreadsheets/d/1ltLccexTQzTCyV8gCaGA1_rlHbDEIaGwSntXX6zvQ0U/edit#gid=0
    Link to Appscript that is called by the app: https://script.google.com/a/avanti.in/d/1hzxh62XVeKfhZ57_6ecPSF7OH_a54pxglSAEMNJHVK32eUgJntLOssIR/edit?

    Reference for implementation of Google Sheets adding: https://www.youtube.com/watch?v=v2Az8yIU1lE
     */
    private fun updateLocationSpreadsheet() {
        // For now we will only get the student's location once
        if (PreferenceHandler.readBoolean(this, SankalpConstants.SHARED_PREF_LOCATION_UPDATE_DONE, false)) {
            return
        }

        val phone = AccountManager.getInstance().loggedInStudent.phone ?: AccountManager.getInstance().loggedInStudent.email

        val params: MutableMap<String, String> = HashMap()

        params["phone"] = phone
        params["latitude"] = wayLatitude.toString()
        params["longitude"] = wayLongitude.toString()
        params["action"] = "addItem"

        val call: Call<String> = GoogleSheetsScriptApiClient.instance.sankalpApiService.addRow(params)

        call.enqueue(
                object : Callback<String> {
                    override fun onResponse(call: Call<String>, response: Response<String>) {
                        if (response.isSuccessful) {
                            Log.d(LOG_TAG, "Successfully updated location")
                            PreferenceHandler.writeBoolean(this@SankalpMainActivity, SankalpConstants.SHARED_PREF_LOCATION_UPDATE_DONE,
                                    true);
                        } else {
                            Log.d(LOG_TAG, "Failed to update location")
                        }
                    }

                    override fun onFailure(call: Call<String>, t: Throwable) {
                        Log.d(LOG_TAG, "Error while connecting to sheets: $t")
                    }
                })
    }

    private fun getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    LOCATION_REQUEST)
        } else {
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_REQUEST) { // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLocation()
            } else {
                Toast.makeText(this, R.string.location_permission_needed, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GpsUtils.GPS_REQUEST) {
                isGPSOn = true // flag maintain before get location
                updateLocation()
            }
        }
    }
}