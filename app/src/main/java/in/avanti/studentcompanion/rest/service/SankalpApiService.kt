package `in`.avanti.studentcompanion.rest.service

import retrofit2.Call
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface SankalpApiService {
    @POST("exec")
    @FormUrlEncoded
    fun addRow(@FieldMap locationParams: Map<String, String>): Call<String>
}