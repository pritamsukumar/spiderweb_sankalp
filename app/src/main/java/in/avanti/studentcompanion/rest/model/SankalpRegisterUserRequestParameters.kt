package `in`.avanti.studentcompanion.rest.model

class SankalpRegisterUserRequestParameters(user: SankalpUserData, context: String) :
        RegisterUserRequestParameters<SankalpRegisterUserRequestParameters.SankalpUserData>(user, context) {

    class SankalpUserData(internal val grade: String, private val curriculum: String, userDataBuilder: UserDataBuilder)
        : UserData(userDataBuilder)
}