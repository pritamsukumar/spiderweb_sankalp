package `in`.avanti.studentcompanion.rest.client

import `in`.avanti.studentcompanion.SankalpAppConfig

class GoogleSheetsScriptApiClient private constructor() : SankalpApiClient() {

    init {
        initialize(SankalpAppConfig.GOOGLE_SHEETS_SCRIPT_URL, false, false)
    }

    companion object {
        private var sInstance: GoogleSheetsScriptApiClient? = null

        val instance: GoogleSheetsScriptApiClient
            get() {
                if (sInstance == null) {
                    sInstance = GoogleSheetsScriptApiClient()
                }
                return sInstance as GoogleSheetsScriptApiClient
            }
    }
}
