package `in`.avanti.studentcompanion.rest.client

import `in`.avanti.studentcompanion.rest.service.SankalpApiService
import retrofit2.Retrofit

abstract class SankalpApiClient : ApiClient() {

    internal lateinit var sankalpApiService: SankalpApiService

    override fun createService(retrofit: Retrofit) {
        sankalpApiService = retrofit.create(SankalpApiService::class.java)
    }
}